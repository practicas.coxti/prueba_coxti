{\rtf1\ansi\ansicpg1252\deff0\nouicompat\deflang3082{\fonttbl{\f0\fnil\fcharset0 Calibri;}}
{\*\generator Riched20 10.0.19041}\viewkind4\uc1 
\pard\sa200\sl276\slmult1\qc\f0\fs22\lang10\par

\pard\sa200\sl276\slmult1\b Nombre\b0 : Tu Credito\par
\par
\b Descripcion\b0 : Este es un sistema es para el registro de usuarios que quieren aplicar a un credito pero antes deben de ingresar unos datos personales y financieros, en la primera parte del programa deben ingresar un correo con un password. El mismo sistema se encarga de validar que la contrase\'f1a que se esta ingresando sea la misma mediante una confirmacion de un segundo campo que pide que se ingrese nuevamente la contrase\'f1a esto con un fin de que su contrase\'f1a sea unica, tiene un login de usuarios que despues de completar este primer registro mencionado los direccionan a login de usuarios deben de tener el correo y password con el que hicieron su registro de lo contrario tendra un alerta que les dira que no pueden continuar ya que los datos ingresados son incorrectos su correo y contrase\'f1a. \par
Si los datos son correctos puede continuar con el flujo el aplicante e ingresa a un sistema  donde debe hacer el registro de sus datos personales y datos de residencia como: \par
(Nombre completo,numero de identificacion,correo electronico,departamento,ciudad,barrio y direccion de residencia). Debe llenar todos los campos ya que todos son obligatorios para poder continuar con el flujo.\par
Luego de completado el paso anterior satisfactoriamente el sistema los dejara continuar y los lleva para que puedan ingresar los datos financieros como: \par
(salario,otros ingresos,gastos mensuales y gastos financieros). En este paso se ingresa el salario del aplicante para que el sistema le de una respuesta del valor del salario.\par
Debe ingresar todos los datos comentados anteriormente para que sea finalizado el registro con exito.\par
\par
\b Estrategias: \par
\b0 1\b . \b0 Definir los requerimientos que esta solicitando el cliente.\par
2.Implementar un sistema en el que se tuviera un registro de los aplicantes.\par
3.Crear una base datos en donde se almacenan todos sus datos requeridos.\par
4.Se busco de que el sistema sea de facil acceso y manejo tambien para que cualquier persona lo pueda utilizar.\par
5.El sistema se le implementa una validacion de correo y contrase\'f1a esto con el fin de que sea mas confiable y seguro.\par
6.Se realizo un  sistema con campos claros y de buena estetica y letra legible.\par
7.Se busco que la persona ingrese su sueldo y este le haga su calculo.\par
8.Tiene mensajes informativos en donde le informa al aplicante su paso a paso.\par
\par
 \b Dificultades\b0 : \par
 Tuve  incovenientes con el backend ya que al momento de realizar las migraciones me estaba sacando un error y este impedia tambien que pudiera realizar consultas en insomnia y postman y era porque estaba llamando el modelo diferente al nombre de la tabla de la Base de datos.\par
Otra dificultada fue que trabaje con boostrap y no pude ingresar material y este es muy esencial ya que da buenas herramientas para  web m\'e1s creativos, centrados en la apariencia y que requieren de animaciones o dise\'f1os m\'e1s especiales\par
Tambien hubo un incoveniente con la creacion de la API ya que era primera vez que creaba una y con el calculo del sueldo del tercer paso.\par
\par
\par
}