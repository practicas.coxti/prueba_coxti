import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { User } from '../models/user';
import { Regist } from '../models/regist';
import {  Financials } from '../models/financial';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
@Output() sendDatas: EventEmitter<any> = new EventEmitter();
apiURL = environment.apiURL;

  constructor(
    private http : HttpClient
  ) { }

  register(user: User): Observable<any>{
    return this.http.post(`${this.apiURL}users`,user)

  }

  login(user: User): Observable<any>{
    return this.http.post(`${this.apiURL}login`,user)

  }

  registers(regist: Regist): Observable<any>{
    return this.http.post(`${this.apiURL}registers`,regist)
  }

  financials(financial: Financials): Observable<any>{
    return this.http.post(`${this.apiURL}financials`,financial)
  }
  

 
}
