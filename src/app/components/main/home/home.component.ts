import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { errorMessage, successDialog } from 'src/app/functions/alerts';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { Regist } from '../../../models/regist';




export interface modalData {
  first_name: string;
  second_name: string;
  first_lastname: string;
  second_lastname: string;
  cellphone:number;
  email: string;
  departament: string;
  city: string;
  quarter: string;
  address: string
}




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  registerForm: FormGroup 
  user:User;
  regist:Regist;
  first_name: string;
  second_name: string;
  first_lastname: string;
  second_lastname: string;
  cellphone: number;
  email: string;
  departament: string;
  city: string;
  quarter: string;
  address:  string;
  dataEntrante: string

 


  

  constructor(
    private fb:FormBuilder,
    private authService:AuthService,
    private router: Router,

    
  ) { 
    this.createdFrom();
   
    
   }


   createdFrom():void{
    this.registerForm = this.fb.group({
      first_name:['',[Validators.required, Validators.pattern('[A-ZÑa-zñÀ-ÿ]+[A-ZÑa-zñÀ-ÿ ]*')]],
      second_name:['',[Validators.required, Validators.pattern('[A-ZÑa-zñÀ-ÿ]+[A-ZÑa-zñÀ-ÿ ]*')]],
      first_lastname:['',[Validators.required, Validators.pattern('[A-ZÑa-zñÀ-ÿ]+[A-ZÑa-zñÀ-ÿ ]*')]],
      second_lastname:['',[Validators.required, Validators.pattern('[A-ZÑa-zñÀ-ÿ]+[A-ZÑa-zñÀ-ÿ ]*')]],
      cellphone:['',[Validators.required,Validators.minLength(10), Validators.maxLength(10),]],
      email:['',[Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]],
      departament:['',[Validators.required, Validators.pattern('[A-ZÑa-zñÀ-ÿ]+[A-ZÑa-zñÀ-ÿ ]*')]],
      city:['',[Validators.required, Validators.pattern('[A-ZÑa-zñÀ-ÿ]+[A-ZÑa-zñÀ-ÿ ]*')]],
      quarter:['',[Validators.required, Validators.pattern('[A-ZÑa-zñÀ-ÿ]+[A-ZÑa-zñÀ-ÿ ]*')]],
      address:['',[Validators.required,]]
     
    });

  }

  ngOnInit(): void {
  }

  setUser():void{
    this.regist = {
      first_name: this.registerForm.get('first_name').value,
      second_name: this.registerForm.get('second_name').value,
      first_lastname: this.registerForm.get('first_lastname').value,
      second_lastname: this.registerForm.get('second_lastname').value,
      cellphone: this.registerForm.get('cellphone').value,
      email: this.registerForm.get('email').value,
      departament: this.registerForm.get('departament').value,
      city: this.registerForm.get('city').value,
      quarter:  this.registerForm.get('quarter').value,
      address: this.registerForm.get('address').value

    }
   }

  register():void{
  //console.log(this.registerForm.value)
    if(this.registerForm.invalid){
     return Object.values(this.registerForm.controls).forEach(control => {
       control.markAllAsTouched()
     }); 
   }else{
     this.setUser();
     this.authService.registers(this.regist).subscribe((data:any) => {
        successDialog('Datos registrados con exito TuCRedito.com desde - 2022');
        this.router.navigate(['/financial']);  
     }, error =>  {
       errorMessage('ha ocurrido un error')
     });
   }

   }

   

   
   get firstNameValidate(){
    return(
      this.registerForm.get('first_name').invalid && this.registerForm.get('first_name').touched  
    );
    
  }

  get secondNameValidate(){
    return(
      this.registerForm.get('second_name').invalid && this.registerForm.get('second_name').touched
    );
  }

  get firstLastNameValidate(){
    return(
      this.registerForm.get('first_lastname').invalid && this.registerForm.get('first_lastname').touched
    );
  }

  get secondLastNameValidate(){
    return(
      this.registerForm.get('second_lastname').invalid && this.registerForm.get('second_lastname').touched
    );
  }

  get cellphoneValidate(){
    return(
      this.registerForm.get('cellphone').invalid && this.registerForm.get('cellphone').touched
    );
  }

  get emailValidate(){
    return(
      this.registerForm.get('email').invalid && this.registerForm.get('email').touched
    );
  }

  
  get departamentValidate(){
    return(
      this.registerForm.get('departament').invalid && this.registerForm.get('departament').touched
    );
  }

  get cityValidate(){
    return(
      this.registerForm.get('city').invalid && this.registerForm.get('city').touched
    );
  }

  get quarterValidate(){
    return(
      this.registerForm.get('quarter').invalid && this.registerForm.get('quarter').touched
    );
  }

  get addressValidate(){
    return(
      this.registerForm.get('address').invalid && this.registerForm.get('address').touched
    );
  }






    

   

    
  }
  
  


 

 

  


