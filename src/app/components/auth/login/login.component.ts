import { Component, OnInit } from '@angular/core';
import { ControlContainer, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { AuthService } from '../../../services/auth.service';
import { errorMessage, successDialog } from '../../../functions/alerts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginform: FormGroup;
  user:User;

  constructor(
    private fb:FormBuilder,
    private authservice: AuthService,
    private router: Router
  ) { 
    this.createdForm();
  }

  ngOnInit(): void {
  }

  login():void{
    if(this.loginform.invalid){
      return Object.values(this.loginform.controls).forEach(control => {
        control.markAllAsTouched()
      }); 
    }else{
      this.setUser();
      this.authservice.login(this.user).subscribe((data:any) => {
        successDialog('iniciaste sesion  TuCredito.com desde - 2022').then(() => {
        this.router.navigate(['/home']);
      });
    
    
  }, error => {
        errorMessage('usuario y contraseña incorrecta');

    });

      
    }
  }

  

  get emailValidate(){
    return(
      this.loginform.get('email').invalid && this.loginform.get('email').touched
    );
  }

  get passwordValidate(){
    return(
      this.loginform.get('password').invalid && this.loginform.get('password').touched
    );
  }

  createdForm(){
    this.loginform = this.fb.group({
      email:['',[Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]],
      password:['',[Validators.required]],
    })
  }

  setUser():void{
    this.user = {
      email: this.loginform.get('email').value,
      password: this.loginform.get('password').value
    }
   }

}
