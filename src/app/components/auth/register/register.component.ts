import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { AuthService } from '../../../services/auth.service';
import { successDialog, errorMessage } from '../../../functions/alerts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  user:User;

  constructor(
    private fb:FormBuilder,
    private authService:AuthService,
    private router: Router
  ) { 
    this.createdFrom();
  }

  ngOnInit(): void {
  }

  register():void{
   if(this.registerForm.invalid){
    return Object.values(this.registerForm.controls).forEach(control => {
      control.markAllAsTouched()
    }); 
  }else{
    this.setUser();
    this.authService.register(this.user).subscribe((data:any) => {
       successDialog('registro completado ¡Bienvenido a TuCredito.com desde - 2022!');
       this.router.navigate(['/login']);
       
    }, error =>  {
      errorMessage('ha ocurrido un error')

    });
    
  }

  }

  createdFrom():void{
    this.registerForm = this.fb.group({
      email:['',[Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]],
      password:['',[Validators.required]],
      password2:['',[Validators.required]]
    });

  }

  get emailValidate(){
    return(
      this.registerForm.get('email').invalid && this.registerForm.get('email').touched
    );
  }

  get passwordValidate(){
    return(
      this.registerForm.get('password').invalid && this.registerForm.get('password').touched
    );
  }

  get password2Validate(){

    const pass = this.registerForm.get('password').value;
    const pass2 = this.registerForm.get('password2').value;

    return pass === pass2 ? false : true;

    
  }

  setUser():void{
     this.user = {
       email: this.registerForm.get('email').value,
       password: this.registerForm.get('password').value
     }
    }
  

  }
