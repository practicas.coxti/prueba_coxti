import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { errorMessage, successDialog } from '../functions/alerts';
import { Financials} from '../models/financial';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-financial',
  templateUrl: './financial.component.html',
  styleUrls: ['./financial.component.css']

})

export class FinancialComponent implements OnInit {

 

  financialForm: FormGroup;
  numericPattern = '^[0-9]+$';
  financial: Financials;
  salary: number;
  other_income: number;
  monthly_expenses: number;
  financial_expenses:number;
  result: number




  constructor(
    private fb:FormBuilder,
    private authService:AuthService,
    private router: Router
    
    

  ) { 
    this.createForm();
  }

  ngOnInit(): void {
  }

  setUser(): void {
    this.financial = {
    salary: this.financialForm.get('salary').value,
    other_income: this.financialForm.get('other_income').value,
    monthly_expenses: this.financialForm.get('monthly_expenses').value,
    financial_expenses: this.financialForm.get('financial_expenses').value,
    result: this.financialForm.get('result').value
    }
  }




    
   createForm():void{
    this.financialForm = this.fb.group({
      salary:['',[Validators.required, Validators.min(0), Validators.max(9999999999999999999),Validators.pattern(this.numericPattern)]],
      other_income:['',[Validators.required, Validators.min(0), Validators.max(9999999999999999999),Validators.pattern(this.numericPattern)]],
      monthly_expenses:['',[Validators.required, Validators.min(0), Validators.max(9999999999999999999),Validators.pattern(this.numericPattern)]],
      financial_expenses:['',[Validators.required,Validators.min(0), Validators.max(9999999999999999999),Validators.pattern(this.numericPattern)]],
      result:['',[Validators.required,Validators.min(0), Validators.max(9999999999999999999),Validators.pattern(this.numericPattern)]]
  
     
    });

  }

  register():void{
    if(this.financialForm.invalid){
      return Object.values(this.financialForm.controls).forEach(control => {
        control.markAllAsTouched()
      }); 
    }else{
      this.setUser();
      this.authService.financials(this.financial).subscribe((data:any) => {
         successDialog('Has finalizado tu registro ¡Felicidades! TuCredito.com desde - 2022');
         this.router.navigate(['/modal']);
         
      }, error =>  {
        errorMessage('ha ocurrido un error')
      });
      
    }
  
   
   
  }


  calcular():void{
   
    this.result = (this.salary-this.other_income)+(this.financial_expenses-this.monthly_expenses)

  }



  

  get SalaryValidate(){
    return(
      this.financialForm.get('salary').invalid && this.financialForm.get('salary').touched
    );
  }

  
  get other_incomeValidate(){
    return(
      this.financialForm.get('other_income').invalid && this.financialForm.get('other_income').touched
    );
  }

  
  get monthly_expensesValidate(){
    return(
      this.financialForm.get('monthly_expenses').invalid && this.financialForm.get('monthly_expenses').touched
    );
  }

  get financial_expensesValidate(){
    return(
      this.financialForm.get('financial_expenses').invalid && this.financialForm.get('financial_expenses').touched
    );
  }

  get resultValidate(){
    return(
      this.financialForm.get('result').invalid && this.financialForm.get('result').touched
    );
  }

  

  
  

  

}
